const express = require('express');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser =  require('body-parser');
const { EstudianteRoute, AuthRoute, UsuarioRoute } = require('./routes');

// CONEXION
const mongoose = require('mongoose');
const { db } = require('./config');
mongoose.connect(db.urlConexion, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set('debug', true);

app.use(cors());

app.use(bodyParser.json());

app.use(EstudianteRoute);
app.use(AuthRoute);
app.use(UsuarioRoute);


app.listen(port, () => {
  console.log(`===> Funcionando en el puerto ${port}`);
});
