const mongoose = require('mongoose');
const { Schema } = mongoose;

const UsuarioSchema = new Schema({
  usuario: String,
  contrasena: String,
  permisos: Array
});

const Usuario = mongoose.model('usuario', UsuarioSchema);

module.exports = Usuario;
