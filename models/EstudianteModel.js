const mongoose = require('mongoose');
const { Schema } = mongoose;

const EstudianteSchema = new Schema({
  nombres: String,
  apellidoPaterno: String,
  apellidoMaterno: String,
  numeroDocumento: Number,
  correoElectronico: String
});

const Estudiante = mongoose.model('estudiante', EstudianteSchema);

module.exports = Estudiante;
