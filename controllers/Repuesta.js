const mensajeExito = (res, code = 200, mensaje = 'OK', datos = {}) => {
  return res.status(code).json(
    {
      finalizado: true,
      mensaje,
      datos
    }
  )
};

const mensajeError = (res, code, mensaje, datos) => {
  return res.status(code).json(
    {
      finalizado: false,
      mensaje,
      datos: datos
    }
  );
}


module.exports = {
  mensajeExito,
  mensajeError
};