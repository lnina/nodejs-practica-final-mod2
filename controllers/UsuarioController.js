const { app, constants } = require('../config');
const axios = require('axios');
const { UsuarioService } = require('../services');
const { mensajeExito, mensajeError } = require('./Repuesta');

const crearUsuario = async (req, res) => 
{
  try 
  {
    const datos = req.body;
    const usuarioCreado = await UsuarioService.guardar(datos);
    return mensajeExito(res, 201, 'Usuario creado', usuarioCreado);
  }
  catch (error) 
  {
    return mensajeError(res, 400, error.message, null);
  }
};

const actualizarUsuario = async (req, res) => 
{
  try 
  {
    const { _id } = req.params;
    const datos = req.body;
    datos._id = _id;
    const usuarioActualizado = await UsuarioService.guardar(datos);
    return mensajeExito(res, 201, 'Usuario modificada correctamente', usuarioActualizado);
  }
  catch (error) 
  {
    return mensajeError(res, 400, error.message, null);
  }
};

const actualizarUsuarioPermisos = async (req, res) => 
{
  try 
  {
    const { _id } = req.params;
    const datos = req.body;
    datos._id = _id;
    const usuarioPermisosActualizado = await UsuarioService.guardarPermisos(datos);
    return mensajeExito(res, 201, 'Permisos Usuario modificados correctamente', usuarioPermisosActualizado);
  }
  catch (error) 
  {
    return mensajeError(res, 400, error.message, null);
  }
};

const listarUsuarios = async (req, res) => 
{
  try 
  {
    const { numeroDocumento } = req.query;
    const usuariosListados = await UsuarioService.findAll({ numeroDocumento: numeroDocumento });
    return mensajeExito(res, 200, 'OK', usuariosListados);
  }
  catch (error)
  {
    return mensajeError(res, 400, error.message, null)
  }
};

const eliminarUsuario = async (req, res) => 
{
  try 
  {
    const { _id } = req.params;
    const usuarioEliminado = await UsuarioService.eliminar(_id);
    return mensajeExito(res, 200, 'Usuario eliminada correctamente', usuarioEliminado);
  }
  catch (error) 
  {
    return mensajeError(res, 400, error.message, null);
  }
};

const login = async (req, res) => 
{
  try 
  {
    const { usuario, contrasena } = req.body;
    const respuesta = await UsuarioService.login(usuario, contrasena);
    return mensajeExito(res, 200, 'Usuario identificado correctamente', respuesta);
  }
  catch (error) 
  {
    return mensajeError(res, 400, error.message, null);
  }
}

module.exports = 
{
  listarUsuarios,
  crearUsuario,
  actualizarUsuario,
  actualizarUsuarioPermisos,
  eliminarUsuario,
  login
};