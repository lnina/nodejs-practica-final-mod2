const { app, constants } = require('../config');
const axios = require('axios');
const { EstudianteService } = require('../services');
const { mensajeExito, mensajeError } = require('./Repuesta');

const crearEstudiante = async (req, res) =>
{
  try
  {
    const datos = req.body;
    const estudianteCreado = await EstudianteService.guardar(datos);
    return mensajeExito(res, 201, 'Estudiante creado', estudianteCreado);
  }
  catch (error)
  {
    return mensajeError(res, 400, error.message, null);
  }
};

const actualizarEstudiante = async (req, res) =>
{
  try
  {
    const { _id } = req.params;
    const datos = req.body;
    datos._id = _id;
    const estudianteCreado = await EstudianteService.guardar(datos);
    return mensajeExito(res, 201, 'Estudiante modificado correctamente', estudianteCreado);
  }
  catch (error)
  {
    return mensajeError(res, 400, error.message, null);
  }
};

const listarEstudiantes = async (req, res) =>
{
  try
  {
    const { idUsuario } = req.query;
    const listaEstudiantes = await EstudianteService.findAll({ usuarioCreacion: idUsuario });
    return mensajeExito(res, 200, 'OK', listaEstudiantes);
  }
  catch (error)
  {
    return mensajeError(res, 400, error.message, null)
  }
};


const eliminarEstudiante = async (req, res) =>
{
  try
  {
    const { _id } = req.params;
    const estudianteEliminado = await EstudianteService.eliminar(_id);
    return mensajeExito(res, 201, 'Estudiante eliminado correctamente', estudianteEliminado);
  }
  catch (error)
  {
    return mensajeError(res, 400, error.message, null);
  }
};


module.exports = {
  listarEstudiantes,
  crearEstudiante,
  actualizarEstudiante,
  eliminarEstudiante
};