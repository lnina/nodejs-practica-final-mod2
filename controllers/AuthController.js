const { app, constants } = require('../config');
const { mensajeExito, mensajeError } = require('./Repuesta');
const { UsuarioService } = require('../services');
const jwt = require('jsonwebtoken');

const login = async (req, res) => 
{
  try 
  {
    const { usuario, password } = req.body;
    const respuesta = await UsuarioService.login(usuario, password);

    if (!respuesta) 
    {
      throw new Error('Error con sus credenciales de acceso.');
    }

    delete respuesta.password;
    
    const token = jwt.sign({...respuesta,}, app.auth.secret, { expiresIn: '4h' });

    return mensajeExito(res, 200,'Ingreso al sistema de forma correcta.',{ ...respuesta, token });
  }
  catch (error)
  {
    return mensajeError(res, 400, error.message, null);
  }
};

module.exports = 
{
  login
};