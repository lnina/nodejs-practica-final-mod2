const express = require('express');
const router = express.Router();
const { EstudianteController } = require('../controllers');
const { AuthMiddleware } = require('../middlewares');

//router.get('/estudiantes', AuthMiddleware.verificarToken, EstudianteController.listarEstudiantes);
router.get('/estudiantes',EstudianteController.listarEstudiantes);
router.post('/estudiantes', EstudianteController.crearEstudiante);
router.put('/estudiantes/:_id', EstudianteController.actualizarEstudiante);
router.delete('/estudiantes/:_id', EstudianteController.eliminarEstudiante);

module.exports = router;