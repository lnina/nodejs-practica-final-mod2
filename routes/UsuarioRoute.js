const express = require('express');
const router = express.Router();
const { UsuarioController } = require('../controllers');
const { AuthMiddleware, PermisosMiddleware } = require('../middlewares');

router.get('/usuarios', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos('usuarios:listar'), UsuarioController.listarUsuarios);
router.post('/usuarios',AuthMiddleware.verificarToken, UsuarioController.crearUsuario);
router.put('/usuarios/:_id', AuthMiddleware.verificarToken, UsuarioController.actualizarUsuario);
router.put('/usuarios/permisos/:_id', AuthMiddleware.verificarToken, UsuarioController.actualizarUsuarioPermisos);
router.delete('/usuarios/:_id', AuthMiddleware.verificarToken, UsuarioController.eliminarUsuario);

module.exports = router;