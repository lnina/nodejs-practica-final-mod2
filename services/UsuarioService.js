const { UsuarioRepository } = require('../repositories');

const login = async (usuario, password) => {
  const existeUsuario = await UsuarioRepository.findOne({ usuario, password });
  return existeUsuario;
}

const findAll = async (query) => {
  const personas = await UsuarioRepository.findAndCountAll(query);
  return personas;
}

const guardar = async (datos) => {
  const persona = await UsuarioRepository.createOrUpdate(datos);
  return persona;
}

const guardarPermisos = async (datos) => {
  const persona = await UsuarioRepository.updatePermisos(datos);
  return persona;
}

const eliminar = async (_id) => {
  const personaEliminada = await UsuarioRepository.deleteItem(_id);
  return personaEliminada;
} 

module.exports = {
  findAll,
  guardar,
  guardarPermisos,
  eliminar,
  login
}