const { EstudianteRepository } = require('../repositories');

const findAll = async (query) => 
{
  const estudiantes = await EstudianteRepository.findAndCountAll(query);
  return estudiantes;
}

const guardar = async (datos) => 
{
  const estudiante = await EstudianteRepository.createOrUpdate(datos);
  return estudiante;
}

const eliminar = async (_id) => 
{
  const estudianteEliminado = await EstudianteRepository.deleteItem(_id);
  return estudianteEliminado;
} 

module.exports = 
{
  findAll,
  guardar,
  eliminar
}