const { EstudianteModel } = require('../models');
const Repository = require('./Repository');

module.exports = {
  findAndCountAll: query => Repository.findAndCountAll(EstudianteModel, query),
  createOrUpdate: datos => Repository.createOrUpdate(EstudianteModel, datos),
  deleteItem: _id => Repository.deleteItem(EstudianteModel, _id)
}