const findAndCountAll = async (modelo, query) => {
  const count = await modelo.count(query);
  const rows = await modelo.find(query);
  return { count, rows };
}

const createOrUpdate = async (modelo, datos) => {
  if (datos._id) {
    const _existeRegistro = await modelo.findById(datos._id)
    if (_existeRegistro) {
      const _registroActualizado = await modelo.update({ _id: datos._id }, { $set: datos });
      return _registroActualizado;
    } else {
      const _registroCreado = await modelo.create(datos);
      return _registroCreado;
    }
  }

  const _registroCreado = await modelo.create(datos);
  return _registroCreado;
}



const updatePermisos = async (modelo, datos) =>
{
  if (datos._id)
  {
    const _existeRegistro = await modelo.findById(datos._id)
    if (_existeRegistro)
    {
      const _registroActualizado = await modelo.update({ _id: datos._id }, { $set: datos });
      return _registroActualizado;
    }
    else
    {
      return null;
    }
  }
}

const deleteItem = (modelo, _id) => {
  return modelo.findByIdAndRemove(_id);
}

const findOne = async (modelo, params) => {
  const respuesta = await modelo.findOne(params);
  if (!respuesta) {
    return null;
  }
  return respuesta._doc;
}

module.exports = {
  findAndCountAll,
  createOrUpdate,
  updatePermisos,
  deleteItem,
  findOne
}
