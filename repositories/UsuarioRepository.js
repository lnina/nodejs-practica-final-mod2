const { UsuarioModel } = require('../models');
const Repository = require('./Repository');

module.exports = {
  findOne: params => Repository.findOne(UsuarioModel, params),
  findAndCountAll: query => Repository.findAndCountAll(UsuarioModel, query),
  createOrUpdate: datos => Repository.createOrUpdate(UsuarioModel, datos),
  updatePermisos: datos => Repository.updatePermisos(UsuarioModel, datos),
  deleteItem: _id => Repository.deleteItem(UsuarioModel, _id)
}