
module.exports = 
{
  verificarPermisos: (permiso) => async (req, res, next) => 
  {
    try 
    {
      const { permisos } = req.usuario
      if (!permisos.includes(permiso)) 
      {
        throw new Error('No tiene los permisos necesarios para realizar esta operacion.');
      }
      next()
    }
    catch (error) 
    {
      res.status(401).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    }
  }
}
