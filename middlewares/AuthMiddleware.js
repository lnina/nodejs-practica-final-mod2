const jwt = require('jsonwebtoken');
const { app } = require('../config');
const verificarToken = async (req, res, next) =>
{
  try
  {
    const { authorization } = req.headers;
    if (!authorization)
    {
      throw new Error('No autorizado.');
    }
    const onlyToken = authorization.replace('Bearer ', '')
    const tokenDecodificado = await jwt.verify(onlyToken, app.auth.secret)
    req.query.idUsuario = tokenDecodificado._id
    req.usuario = tokenDecodificado
    
    next();
  }
  catch (error)
  {
    res.status(401).json({
      finalizado: false,
      mensaje: error.message,
      datos: null
    });
  }
}

module.exports = 
{
  verificarToken
}